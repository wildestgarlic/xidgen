package main

import (
	"fmt"
	"math"
	"os"
	"strconv"

	"github.com/rs/xid"
)

const (
	maxIterations     = 10_000
	defaultIterations = 1
)

func main() {
	var iterations = defaultIterations

	if len(os.Args) > 1 {
		n, err := strconv.Atoi(os.Args[1])
		if err == nil {
			iterations = n
		}

		if iterations > maxIterations {
			iterations = maxIterations
		}
	}

	if iterations == 1 {
		fmt.Println(xid.New())

		return
	}

	padding := 1 + int(math.Log10(float64(iterations)))

	for i := range iterations {
		fmt.Printf("%0*d. %s\n", padding, i+1, xid.New())
	}
}
