# xidgen

## generate xids (https://github.com/rs/xid)

## how to install
```bash
go install gitlab.com/wildestgarlic/xidgen
```

## how to use
```bash
xidgen

# prints 10 xids
# 01. cpolvlnd5lbicm15096g
# 02. cpolvlnd5lbicm150970
# 03. cpolvlnd5lbicm15097g
# 04. cpolvlnd5lbicm150980
# 05. cpolvlnd5lbicm15098g
# 06. cpolvlnd5lbicm150990
# 07. cpolvlnd5lbicm15099g
# 08. cpolvlnd5lbicm1509a0
# 09. cpolvlnd5lbicm1509ag
# 10. cpolvlnd5lbicm1509b0
```

### or
```bash
xidgen 1

# prints 1 xid
# 01. cpolvlnd5lbicm15096g
```
